## simple gpio controller

### how to use

prepare node.js and npm. and clone it.

	$ git clone https://bitbucket.org/soomtong/rpi-blinker

	$ cd rpi-blinker
	$ npm install

### run

	$ npm start

## license

MIT © soomtong