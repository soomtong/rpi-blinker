const ledController = require('./lib/led-toggler')
const exitHandler = require('./lib/exit-handler')
const buttonController = require('./lib/button-reader')

buttonController.startButtonWatch(ledController.startLedBlink)
exitHandler.bind()