const Gpio = require('onoff').Gpio
const led = new Gpio(17, 'out')	// BCM 17

let state = led.readSync()

console.log('LED state:', state)
