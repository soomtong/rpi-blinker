const Gpio = require('onoff').Gpio
const led = new Gpio(17, 'out')	// BCM 17

let onValue = 1

led.write(onValue, function () {
	console.log('changed LED state to:', onValue)
})