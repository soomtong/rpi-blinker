const config = require('./config')
const Gpio = require('onoff').Gpio
const led = new Gpio(config.led.bcm, config.led.dir)

let runMode = false
let ledState = true

function blink() {
	led.write(convertState(ledState), () => {
		led.read((err, value) => {
			console.log('changed LED state to:', value == 1 ? 'ON' : 'OFF')
		})

		ledState = !ledState
	})

	if (runMode) {
		setTimeout(blink, config.led.interval)
	}
}

function convertState(state) {
	const ON = 1
	const OFF = 0

	return state ? ON : OFF
}

function Blinker(mode) {
	runMode = mode == 'ON'

	blink()
}

module.exports = {
	startLedBlink: Blinker
}