const config = require('./config')
const Gpio = require('onoff').Gpio
const led = new Gpio(config.led.bcm, config.led.dir)	// BCM 17

exports.bind = () => {
	process.on('SIGINT', function () {	
		led.writeSync(0)
		led.unexport()
		console.log('')
		console.log('Exit...')
		process.exit()
	})
}
