const config = require('./config')
const Gpio = require('onoff').Gpio
const button = new Gpio(config.button.bcm, config.button.dir, config.button.edge)

function Reader(blinker) {
	let buttonStatus = button.readSync()
	console.log('current button state is:', convertValue(buttonStatus))
	
	button.unwatchAll()
	button.watch((err, value) => {
		console.log('changed button state to:', convertValue(value))
		blinker(convertValue(value))
	})
}

function convertValue (value) {
	return value == 1 ? 'OFF' : 'ON'
}

module.exports = {
	startButtonWatch: Reader
}